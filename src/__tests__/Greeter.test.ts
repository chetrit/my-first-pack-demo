import { Greeter, addNumber } from '../index';
test('My Greeter', () => {
  expect(Greeter('Carl')).toBe('Hello Carl');
});

test('Add Number',() => {
    expect(addNumber(5, 10)).toBe(15);
})